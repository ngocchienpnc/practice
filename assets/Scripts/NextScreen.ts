import { _decorator, Component, director, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('NextScreen')
export class NextScreen extends Component {
    
    loadGameA(){
        director.loadScene('scene-door')
    }

    Exit(){
        director.loadScene('scene-2d')
    }

    playGame(){
        director.loadScene('play-game')
    }
}


