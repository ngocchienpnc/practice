import { _decorator, Button, Component, director, Director, error, log, Node, Sprite, tween, Vec3, view } from 'cc';
import { NextScreen } from './NextScreen';
const { ccclass, property } = _decorator;

@ccclass('MoveDoor')
export class Move extends Component {
    @property({
        type: Node
    })
    public doorLeftOne: Node | null = null;
    @property({
        type: Node
    })
    public doorRightOne: Node | null = null;
    @property({
        type: Node
    })
    public doorLeftTwo: Node | null = null;
    @property({
        type: Node
    })
    public doorRightTwo: Node | null = null;
    @property({
        type: Sprite
    })
    public imageToHide: Sprite | null = null;
    @property({
        type: Button
    })
    public buttonToHide: Node | null = null;
    @property({
        type: Button
    })
    public buttonExitToHide: Node | null = null;

    @property({
        type: NextScreen
    })
    public nextScreen: NextScreen;

    @property
    public width: any = 100;

    onLoad() {
        let width = view.getDesignResolutionSize().width;
        console.log('width: ' + width);

        if (this.imageToHide && this.buttonToHide) {
            this.imageToHide.node.active = true;
            this.buttonToHide.node.on('click', this.onHideButtonClick, this)
        }
        else {
            error('Image')
        }
    }

    onHideButtonClick() {
        this.moveOpenDoor();
        this.buttonToHide.node.active = false;
        this.buttonExitToHide.node.active = false
        this.imageToHide.node.active = !this.imageToHide.node.active;
    }
    moveOpenDoor() {
        tween(this.doorLeftOne)
            .to(1, { position: new Vec3(this.doorLeftOne.position.x - this.width, this.doorLeftOne.position.y, this.doorLeftOne.position.z) })
            .start();

        tween(this.doorRightOne)
            .to(1, { position: new Vec3(this.doorRightOne.position.x + this.width, this.doorRightOne.position.y, this.doorRightOne.position.z) })
            .start();

        this.scheduleOnce(this.delayDoor, 0.7)

    }

    delayDoor() {
        tween(this.doorLeftTwo)
            .to(1, { position: new Vec3(this.doorLeftTwo.position.x - this.width, this.doorLeftTwo.position.y, this.doorLeftTwo.position.z) })
            .start();

        tween(this.doorRightTwo)
            .to(1, { position: new Vec3(this.doorRightOne.position.x + this.width, this.doorRightTwo.position.y, this.doorRightTwo.position.z) })
            .call(()=> this.loadScenePlGame())
            .start();
    }

    loadScenePlGame(){
        director.loadScene('play-game')
    }
}