import { _decorator, Button, Component, director, EventTouch, Label, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('GamePlay')
export class GamePlay extends Component {

    // @property({
    //     type: Label
    // })
    // public resultLabel: Label

    public choice: string;



    onButtonRockClick() {
        this.choice = 'rock'
        this.onButtonClick()
    }
    onButtonPaperClick() {
        this.choice = 'paper'
        this.onButtonClick()
    }
    onButtonScissorsrClick() {
        this.choice = 'scissors'
        this.onButtonClick()
    }

    onButtonClick() {
        // Lấy lựa chọn của người chơi
        const playerChoice = this.choice;
        console.log("Player choice: " + playerChoice);

        // Tính toán lựa chọn của máy (đơn giản để minh họa)
        const computerChoice = this.getRandomChoice();
        console.log('computer choice' + this.getRandomChoice());
        
        this.getGameResult(playerChoice,computerChoice)
        // Kiểm tra kết quả và hiển thị
        const result = this.getGameResult(playerChoice, computerChoice);
        // this.resultLabel.string = `Result: ${result}`;
    }
    
    getRandomChoice(): string {
        const choices = ['rock', 'paper', 'scissors'];
        const randomIndex = Math.floor(Math.random() * choices.length);
        console.log('aaa');
        
        let result = choices[randomIndex]
        console.log('result:' +result);
        
        
        return result;
    }

    getGameResult(playerChoice: string, computerChoice: string) {
        // Xác định người chiến thắng dựa trên lựa chọn của người chơi và máy
        if (playerChoice === computerChoice) {
            this.loadSceneDrawn()
            
        } else if (
            (playerChoice === 'rock' && computerChoice === 'scissors') ||
            (playerChoice === 'paper' && computerChoice === 'rock') ||
            (playerChoice === 'scissors' && computerChoice === 'paper')
        ) {
            this.loadSceneWin()
            
        } else {
           this.loadSceneLose()
            
        }
    }

    loadSceneWin(){
        director.loadScene('scene-win')
    }
    loadSceneLose(){
        director.loadScene('scene-lose')
    }
    loadSceneDrawn(){
        director.loadScene('scene-lose')
    }
}


