import { _decorator, Component, director, Node, Scene } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('GameCtrl')
export class GameCtrl extends Component {

    @property({
        type: Scene
    })
    public scene: Scene
   
    loadGameA(){
        director.loadScene('scene-door')
    }
}


